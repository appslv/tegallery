//
//  TEGallery.h
//  1up
//
//  Created by  on 12/1/9.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TEPhoto.h"

@protocol TEGalleryDataSource;
@protocol TEGalleryDelegate;
@class TEScrollView;

@interface TEGallery : UIView <UIScrollViewDelegate> {
    CGSize _photoSize;
    CGFloat _spacing;
    id <TEGalleryDataSource> _dataSource;
    id <TEGalleryDelegate> _delegate;
    NSMutableArray *_photos;
    TEScrollView *_scrollView;
    CGFloat _minimumScale;
    CGFloat _maximumScale;
    BOOL _scaleEnabled;
    BOOL _oldCenterIndex;
}

@property (nonatomic) CGSize photoSize;
@property (nonatomic) CGFloat spacing;
@property (nonatomic) CGFloat minimumScale;
@property (nonatomic) CGFloat maximumScale;
@property (nonatomic) BOOL scaleEnabled;
@property (nonatomic, assign) id <TEGalleryDataSource> dataSource;
@property (nonatomic, assign) id <TEGalleryDelegate> delegate;

- (void)reloadData;
- (void)scrollToIndex:(NSInteger)index animated:(BOOL)animated;

@end


@protocol TEGalleryDataSource <NSObject>

- (TEPhoto *)gallery:(TEGallery *)gallery photoAtIndex:(NSInteger)index;
- (NSInteger)numbersOfPhotoWithGallery:(TEGallery *)gallery;

@end


@protocol TEGalleryDelegate <NSObject>

- (void)gallery:(TEGallery *)gallery didSelectPhotoAtIndex:(NSInteger)index;
- (void)gallery:(TEGallery *)gallery didChangeCenterIndex:(NSInteger)index;

@end


@interface TEScrollView : UIScrollView {
}

@end