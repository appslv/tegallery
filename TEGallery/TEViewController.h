//
//  TEViewController.h
//  TEGallery
//
//  Created by  on 12/1/9.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TEGallery.h"

@interface TEViewController : UIViewController <TEGalleryDataSource, TEGalleryDelegate> {
    NSArray *_cards;
    TEGallery *_gallery;
}

@property (nonatomic, retain) IBOutlet TEGallery *gallery;

@end